import random
import pause

from actor import Actor
from canal import Canal
from message import Message
from datetime import timedelta

class Bob(Actor):
    def __init__(self, canal: Canal):
        super().__init__(canal)
        self.canal.subscribeBob(self)

    def generationSecret(self):
        b = random.randint(0, 1)
        content = "Alice" if b else "Bob"
        msg = Message(content)
        # wait
        delta = random.randint(1, 10)
        pause.milliseconds(delta)
        self.posterMessageAnonyme(msg)

    def runProtocol(self, t_start, t_length):
        pause.until(t_start)
        counter = 0
        while counter < t_length:
            counter += 1
            self.generationSecret()
            pause.seconds(1)

        t_end = t_start + timedelta(seconds=t_length)
        t_start_f = int(t_start.strftime("%Y%m%d%H%M%S%f"))
        t_end_f = int(t_end.strftime("%Y%m%d%H%M%S%f"))
        history = self.canal.recupererMessagesAnonymes(t_start_f, t_end_f)
        result = []
        for item in history:
            res = int(not (item.content == "Bob" and item in self.messages))
            result.append(str(res))
        secret = "".join(result)
        print(f"Bob's secret: {secret}")
