# TP2 - Canal de diffusion anonyme
L'objectif du TP est la mise en place d'un canal de diffusion anonyme, c'est à dire, un canal dans lequel l'expéditeur d'un message est anonyme (exemple: une messagerie ne requierant pas d'identification). Pour ce TP, on se place dans un modèle où l’adversaire est passif, autrement dit, il se contente d’écouter les communications sans chercher à frauder (ex: poster de defaux messages).

## Comment utiliser le projet
Cloner le projet
```bash
git clone
https://gitlab.com/kevinsalaun/cyber_security/tp2_anonymous_communication_channel
```

Préparer le virtualenv
```bash
cd tp2_anonymous_communication_channel
virtualenv .env -p python3
source .env/bin/activate
pip install -r requirements
```

Lancer le projet
```bash
python main.py
```

## Analyse du protocole

1. Un canal de diffusion anonyme est une primitive qui peut être utilisée comme brique de base pour réaliser de nombreuses tâches. Imaginer deux autres applications possibles du canal de diffusion anonyme et décrivez les chacune en quelques phrases.
   - Parmis les usages de ce type de solution, on retrouve Bitmessage, un protocole de communication inspiré de Bitcoin créé en 2012, fonctionnant en pair à pair décentralisé et chiffré destiné à permettre d'échanger des messages chiffrés avec un ou plusieurs correspondants sans que leurs identités ne soit retrassable.
   - Un autre exemple d'usage pourrait-être le vote électronique, une personne envoi son scrutin à une machine chargée de la collecte, cette machine doit-être capable de savoir qui à déjà voté mais ne doit pas être en mesure de relier les votes aux votants (exploitation d'un système de signature anonyme). Ce projet avait été poussé dans le début des années 2000 avant d'être abandonné en 2008 en réponses à un rapport dénonçant des failles de sécurité, la maîtrise ayant évoluée, on peut supposé que le projet pourrait revoir le jour.

2. Argumenter sur le fait que suite au protocole décrit dans la deuxième partie, Alice et Bob se retrouvent en possession d’un secret sur lequel l’adversaire n’a aucune information. Pourquoi est ce le cas ?
   - Un adversaire passif vera des messages passés dont le contenu fait référence à deux primitives, ces deux primitives étant envoyées par les deux interlocuteurs (Alice et Bob) à la fois, rien ne peut-être déduis quant à l'identités des expéditeurs. 
   - Seuls Alice et Bob pourront exploités les primitives échangées pour trouver le secret car ils connaissent les primitives et savent qui ils sont (à titre individuel). 

3. Expliquer en quelques mots pourquoi il est important d’avoir des primitives permettant à deux entités de générer un secret commun à travers un canal de communication qui est potentiellement surveillé.
   - L'usage de primitive pour déterminer un secret commun permet de ne jamais faire transiter celui-ci (ne jamais le divulguer). Si le secret était découvert par un tiers, celui-ci pourrait alors déchiffrer tout les messages échangés entre les interlocuteurs concernés.
