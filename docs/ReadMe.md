# TP2 - Anonymous communication channel
The objective of the TP is to set up an anonymous distribution channel, i.e. a channel in which the sender of a message is anonymous (e.g. a message that does not require identification). For this TP, we place ourselves in a model where the adversary is passive, in other words, he simply listens without trying to cheat (e.g. posting false messages).

## How to use
Cloning the project
```bash
git clone
https://gitlab.com/kevinsalaun/cyber_security/tp2_anonymous_communication_channel
```

Preparing the virtualenv
```bash
cd tp2_anonymous_communication_channel
virtualenv .env -p python3
source .env/bin/activate
pip install -r requirements
```

Launching the project
```bash
python main.py
```

## Protocol analysis
1. An anonymous broadcast channel is a primitive that can be used as a building block for many tasks. Imagine two other possible applications of the anonymous broadcast channel and describe each in a few sentences.
   - Among the uses of this type of solution we find Bitmessage, a communication protocol inspired by Bitcoin created in 2012, operating as a decentralized and encrypted peer-to-peer system designed to allow the exchange of encrypted messages with one or more correspondents without their identities being retraceable.
   - Another example of use could be electronic voting, a person sends his vote to a machine in charge of the collecting, this machine must be able to know who has already voted but must not be able to link the votes to the voters (exploitation of an anonymous signature system). This project had been pushed in the early 2000s before being abandoned in 2008 in response to a report denouncing security breaches. As expertise has evolved, it can be assumed that the project could be revived.

2. Discuss the fact that, as a result of the protocol described in Part 2, Alice and Bob find themselves in possession of a secret about which the opponent has no information. Why is this the case?
   - A passive opponent will see past messages whose content refers to two primitives, these two primitives being sent by both interlocutors (Alice and Bob) at the same time, nothing can be deduced about the identity of the senders. 
   - Only Alice and Bob will be able to exploit the exchanged primitives to find the secret because they know the primitives and know who they are (as individuals). 

3. Explain in a few words why it is important to have primitives that allow two entities to generate a common secret through a communication channel that is potentially monitored.
   - The use of primitive to determine a common secret makes it possible to never pass it on (never divulge it). If the secret was discovered by a third party, this one could then decrypt all the messages exchanged between the concerned interlocutors.
