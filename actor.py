from datetime import datetime
from datetime import timedelta
import random

from canal import Canal
from message import Message

class Actor:
    def __init__(self, canal: Canal):
        self.messages = []
        self.canal = canal

    def initProtocol(self):
        delta = random.randint(5, 15)
        t_start = datetime.now() + timedelta(seconds=delta)
        t_length = random.randint(15, 60)
        print(f"generation params: start_delay={delta}s ; length={t_length}s")
        self.canal.receiveInitProtocol(t_start, t_length)

    def posterMessageAnonyme(self, msg: Message):
        self.messages.append(msg)
        self.canal.posterMessageAnonyme(msg)
