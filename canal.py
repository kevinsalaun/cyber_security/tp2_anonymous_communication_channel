from message import Message
from threading import Thread

class Canal:
    def __init__(self):
        self.messages = []

    def receiveInitProtocol(self, t_start, t_length):
        target_alice = self.alice.runProtocol
        thread_alice = Thread(target=target_alice, args=(t_start, t_length, ))
        target_bob= self.bob.runProtocol
        thread_bob = Thread(target=target_bob, args=(t_start, t_length, ))
        
        thread_alice.start()
        thread_bob.start()
        thread_alice.join()
        thread_bob.join()
        print("Protocol execution completed.")

    def posterMessageAnonyme(self, msg: Message):
        print(f'canal received: {msg}')
        self.messages.append(msg)
        self.messages.sort(key=lambda x: x.date, reverse=True)

    def recupererMessagesAnonymes(self, date1, date2):
        result = []
        for item in self.messages:
            if item.date > date1 and item.date < date2:
                result.append(item)
        return result
    
    def subscribeAlice(self, actor):
        self.alice = actor
    
    def subscribeBob(self, actor):
        self.bob = actor
            