from datetime import datetime

class Message:
    def __init__(self, content: object):
        self.content = content
        now = datetime.now()
        self.date = int(now.strftime("%Y%m%d%H%M%S%f"))

    def __str__(self):
        return f"{self.date} -- {self.content}"

    def __eq__(self, other): 
        return (isinstance(other, Message) and self.date == other.date and self.content == other.content)