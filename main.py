from canal import Canal
from alice import Alice
from bob import Bob

def main():
    canal = Canal()
    alice = Alice(canal)
    bob = Bob(canal)
    alice.initProtocol()

if __name__ == "__main__":
    main()
